# GitLab NPM Registry administration **[PREMIUM ONLY]**

>
[Introduced](https://gitlab.com/gitlab-org/gitlab-ee/issues/5934)
in GitLab 11.7. To learn how to use

When the GitLab NPM Registry is enabled, every project in GitLab will have
its own space to store [NPM](https://www.npmjs.com/) packages.

To learn how to use it, see the [user documentation](../user/project/packages/npm_registry.md).

## Enabling the NPM Registry

NOTE: **Note:**
Once enabled, newly created projects will have the Packages feature enabled by
default. Existing projects will need to
[explicitly enabled it](../user/project/packages/npm_registry.md#enabling-the-packages-repository).

To enable the NPM Registry you need to enable [Packages feature](packages.md)
